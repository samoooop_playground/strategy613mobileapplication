import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';

export default class AnswerButton extends React.Component {
	render() {
		return (
			<TouchableOpacity style= {[this.props.style, {marginVertical: 10}]} onPress={this.props.onPress}>
				<View>
					<Text style={{fontSize: 25, color: 'white', textAlign: 'center', marginVertical: 5}}>{this.props.answerText}</Text>
				</View>
			</TouchableOpacity>
		);
	}
}