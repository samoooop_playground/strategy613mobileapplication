import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity, ScrollView, Icon } from 'react-native';
import RadioButtonQuestion from './RadioButtonQuestion';
import questions from './AddictionQuestions';
import AnswerButton from './AnswerButton';

export default class ScrollDownQuestion extends React.Component {

    static navigationOptions = {
        title: 'TERA',
        // header: () => ({
            // left: ( <Icon name={'chevron-left'} onPress={ () => { this.props.navigation.pop(2) } }  /> ),
        // }),
        headerStyle: {
            backgroundColor: 'rgb( 40, 48, 84)',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            flex: 1,
            alignSelf: 'center',
            textAlign: 'center',
        },
        headerRight: (<View></View>)
    }

    render() {
        const { navigation } = this.props;
        console.log('hey');
        variable = navigation.getParam('variable', {});
        console.log(variable);
        return (
            <ImageBackground
                source={require('./resources/img/Background.png')}
                imageStyle={{resizeMode: 'cover'}}
                style= {{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
            >
            <View
                style = {[styles.container]}
            >   
                <View style={{flex: 1}}/>
                <View style={{flex: 16}}>
                <ScrollView
                    style={[styles.roundedBorderView]}
                >
                    <Text style={{fontSize: 30}}>{""}</Text>
                    {
                        questions.questions.map((question, index) => 
                            <RadioButtonQuestion 
                                key= {index} 
                                questionText= {question.question} 
                                multipleChoice= {!!question.multipleChoice} 
                                choices={question.choices} 
                                variable= {variable}
                            />
                        )
                    }
                    <View
                        style= {{alignItems: 'center', justifyContent: 'center' }}
                    >
                        <AnswerButton
                                onPress= {() => {
                                    this.props.navigation.push('Result', {});
                                }}
                                style= {[styles.button]}
                                answerText= {`Submit`}
                        />
                    </View>
                </ScrollView>
                </View>
                <View style={{flex: 1}}/>
            </View>
            </ImageBackground>
        );
    }

}

styles = StyleSheet.create({
    container: {
        // borderRadius: 20,
        flex: 1,
        width: '95%',
        // backgroundColor: 'rgba(0, 0, 0, 0.7)',
        overflow: 'scroll',
    },
    roundedBorderView: {
        borderRadius: 20,
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        overflow: 'scroll',
    },
    questionText: {
        fontSize: 20,
    },
    button: {
        paddingVertical: 0,
        backgroundColor: 'rgb(0, 176, 240)',
        borderRadius: 20,
        width: '40%',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 10},
        shadowOpacity: 0.8,
        shadowRadius: 2,
      }
});