import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';

export default class RadioButtonQuestion extends React.Component {

    constructor(props) {
        super(props);
        this.multipleChoice = this.props.multipleChoice
        this.nChoices = props.choices.length;
        this.state = {
            selected: Array(this.nChoices).fill(false),
        }
    }

    onPress(index) {
        return () => {
            let selected
            if(this.multipleChoice) {
                selected = this.state.selected.slice()
                selected[index] = !selected[index];
            } else {
                selected = Array(this.nChoices).fill(false);
                selected[index] = true;
            }
            this.setState({
                selected: selected
            })
            console.log(selected)
        }
    }

    render() {
        const { variable } = this.props
        console.log('radio')
        console.log(variable)
        return (
            <View>
                <Text style={[styles.questionText, {fontSize: 20, marginHorizontal: 40,}]}>
                    {this.props.questionText.replace('$VEHICLE', variable.VEHICLE)}
                </Text>
                <View 
                    style={{width: '95%', flex: 1, justifyContent: 'center', alignItems: 'center'}}
                >
                    {
                        this.props.choices.map((choice, index) => 
                            <TouchableOpacity 
                                key= {index} 
                                style= {[styles.button, this.state.selected[index] ? styles.selected : {}, {marginVertical: 5}]} 
                                onPress={this.onPress(index)}
                            >
                                <View style= {{flexDirection: 'row'}}>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                    <Image
                                        source= { this.state.selected[index] ? 
                                                        require('./resources/img/selected.png') : 
                                                        require('./resources/img/unselected.png')
                                                }
                                        style= {{
                                                flex: 2,
                                                width: '100%',
                                                height: '100%',
                                                resizeMode: 'contain',
                        
                                                // top: '-50%',
                                                // left: '-50%',
                                        }}
                                    />
                                    </View>
                                    <View style= {{flex: 9}}>
                                        <Text style={{marginVertical: 5, fontSize: 15, color: 'white', textAlign: 'center'}}>
                                            {choice.replace('$VEHICLE', variable.VEHICLE)}
                                        </Text>
                                    </View>
                                    
                                </View>
                            </TouchableOpacity>
                        )
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      paddingVertical: 0,
      backgroundColor: 'rgba(0, 176, 240, 0.2)',
      borderRadius: 20,
      width: '90%',
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.8,
      shadowRadius: 2,
    },
    questionText: {
        fontSize: 30,
        color: 'grey',

    },
    selected: {
        backgroundColor: 'rgba(0, 176, 240, 1)',
    }
  });
  