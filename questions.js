export default questions = {
	'1': {
		question: 'Are you Thai or foreigner?',
		answers: [
			{answer: 'Thai', nextQuestion: '2', isLast: false},
			{answer: 'Foreigner', nextQuestion: '4', isLast: false},
		],
		isLast: false,
	},
	'2': {
		question: 'What is your source of income?',
		answers: [
			{answer: 'No salary (Student)', nextQuestion: '2', isLast: true},
			{answer: 'Regular salary', nextQuestion: '3', isLast: false},
			{answer: 'Self-employed', nextQuestion: '5', isLast: false},
		],
		isLast: false,
	},
	'3': {
		question: 'What is your occupation?',
		answers: [
			{answer: 'Young professional', nextQuestion: '2', isLast: true},
			{answer: `"Community Development"`, nextQuestion: '2', isLast: true},
			{answer: 'Retail bond trader', nextQuestion: '2', isLast: true},
		],
		isLast: true,
	},
	'4': {
		question: 'Foriegner questions',
		answers: [
			{answer: 'Expat', nextQuestion: '2', isLast: true},
			{answer: 'Foreigner looking to buy properties in Thailand', nextQuestion: '2', isLast: true},
		],
		isLast: true,
	},
	'5': {
		question: 'What is your occupation?',
		answers: [
			{answer: 'Taxi driver', nextQuestion: '2', isLast: true, variable: {VEHICLE: 'taxi'}},
			{answer: 'Motorcycle driver', nextQuestion: '2', isLast: true, variable: {VEHICLE: 'motorcycle'}},
			{answer: 'Tuk tuk driver', nextQuestion: '2', isLast: true},
			{answer: 'Street vendor', nextQuestion: '2', isLast: true},
			{answer: 'Young professional', nextQuestion: '2', isLast: true},
			{answer: `"Community Development"`, nextQuestion: '2', isLast: true},
			{answer: 'Retail bond trader', nextQuestion: '2', isLast: true},
		],
		isLast: true,
	}
}