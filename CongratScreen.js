import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';
import AnswerButton from './AnswerButton';

export default class CongratScreen extends React.Component {
    static navigationOptions = {
        header: null,
    }

    // componentDidMount() {
	// 	const { navigation } = this.props;
	// 	this._sub = this.props.navigation.addListener('didFocus',() => {
	// 		run = () => {navigation.push('MoreQuestion', {answered: [], variable: {}})}
	// 		setTimeout(run, 100)	
	// 	})
    // }

    render() {
        const { navigation } = this.props;
        variable = navigation.getParam('variable', {VEHICLE: 'vehicle'});
        // console.log('heys');
        if(!variable.VEHICLE) {
            variable.VEHICLE = 'vehicle'
        }
        return (
            <ImageBackground
            source={require('./resources/img/Congrat.png')}
            imageStyle={{resizeMode: 'cover'}}
            style= {{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
            >
                <View style={{flex: 8}}/>
                    <View style={{flex: 2, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                        <AnswerButton
                                onPress= {() => {
                                    this.props.navigation.push('MoreQuestions', {variable: variable});
                                }}
                                style= {[styles.button]}
                                answerText= {`OK`}
                        />
                </View>
            </ImageBackground>
        );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      paddingVertical: 0,
      backgroundColor: 'rgb(0, 176, 240)',
      borderRadius: 20,
      width: '40%',
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 3},
      shadowOpacity: 0.8,
      shadowRadius: 2,
    }
  });
  