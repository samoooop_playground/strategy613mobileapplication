import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import questions from './questions';
import AnswerButton from './AnswerButton';

export default class QuestionScreen extends React.Component {
    static navigationOptions = {
        title: 'TERA',
        headerStyle: {
            backgroundColor: 'rgb( 40, 48, 84)',
        },
        headerBackTitle: '',
        headerTintColor: '#fff',
        headerTitleStyle: {
            flex: 1,
            alignSelf: 'center',
            textAlign: 'center',
        },
        headerRight: (<View></View>)
    }

    render() {
      const { navigation } = this.props;
      const questionNumber = navigation.getParam('questionNumber', 1);
      const answered = navigation.getParam('answered', []);
      const variable = navigation.getParam('variable', {});
      const question = questions[questionNumber + '']
      // console.log(ansList);
      return (
          <View style={{flex: 1}}>
              <ImageBackground
                  source={require('./resources/img/Background.png')}
                  imageStyle={{resizeMode: 'cover'}}
                  style= {{ flex: 1, alignItems: 'center', justifyContent: 'center', zIndex: 5}}
              >	
                <View style={{flex: 1, width: '100%',}}>
                  <View style= {{flex: 1, width: '100%',}}/>
                  <View
                      style= {{
                          flex: 9,
                          width: '100%',
                          alignItems: 'center', 
                          justifyContent: 'flex-start', 
                          overflow: 'visible',
                      }}
                  >	
                      <View
                          style= {{
                              width: '95%',
                              backgroundColor: 'rgba(0, 0, 0, 0.7)',
                              borderRadius: 20,
                              alignItems: 'center',
                          }}
                      >
                          {/* <View style= {{height: '15%', backgroundColor: 'red'}}/> */}
                          <Text 
                              style= {{
                                          fontSize: 30,
                                          color: 'grey',
                                      }}
                          >	
                          {" "}
                          </Text>
                          <Text 
                              style= {{
                                          fontSize: 30,
                                          color: 'rgb(137, 148, 158)',
                                          textAlign: 'center',
                                      }}
                          >	
                            {question.question}
                          </Text>
                          {
                              question.answers.map((ans, i) => 
                                  <AnswerButton
                                      key= {i}
                                      onPress= {() => {
                                          console.log(ans)
                                          newAnswered = answered.slice()
                                          newAnswered.push(i)
                                          oldVar = variable
                                          newVar = {...oldVar, ...ans.variable}
                                          params =  {
                                              questionNumber: ans.nextQuestion,
                                              answered: newAnswered,
                                              variable: newVar,
                                          }
                                          console.log(params)
                                          this.props.navigation.push(ans.isLast ? 'Congrat' : 'Question', params)
                                  }}
                                      style= {[styles.button]}
                                      answerText= {ans.answer}
                                  />
                              )
                          }
                          <Text 
                              style= {{
                                          fontSize: 30,
                                          color: 'grey',
                                      }}
                          >	
                          {" "}
                          </Text>
                      </View>
                  </View>
                  </View>
              </ImageBackground>
              <View
                  style = {{
                      position: 'absolute',
                      width: '100%',
                      height: '20%',
                      // backgroundColor: 'orange',
                      // opacity: 0.99,
                      justifyContent: 'center',
                      alignItems: 'center',
                      zIndex: 13,
                  }}
              >
                  <View
                      style= {{width: '100%',height: '100%',flex: 2, justifyContent: 'center', alignItems: 'center'}}
                  >
                      <Image
                          source= {require('./resources/img/Logo.png')}
                          style= {{	
                                  flex: 1,
                                  // width: '50%',
                                  resizeMode: 'contain',
                                  // top: '-50%',
                                  // left: '-50%',
                          }}
                      />
                  </View>
              </View>
          </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      paddingVertical: 0,
      backgroundColor: 'rgb(0, 176, 240)',
      borderRadius: 20,
      width: '80%',
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 10},
      shadowOpacity: 0.8,
      shadowRadius: 2,
    }
  });
  