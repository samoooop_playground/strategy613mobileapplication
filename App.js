import React from 'react';
import { View, Text, Button, Image, ImageBackground, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import QuestionScreen from './QuestionScreen';
import questions from './questions';
import AnswerButton from './AnswerButton';
import CongratScreen from './CongratScreen';
import ScrollDownQuestion from './ScrollDownQuestion'

class HomeScreen extends React.Component {

	static navigationOptions = {
		header: null,
	}
	
	componentDidMount() {
		const { navigation } = this.props;
		// this._sub = this.props.navigation.addListener('didFocus',() => {
			// run = () => {navigation.push('Question', {answered: [], variable: {}})}
			// run = () => {navigation.push('Result', {answered: [], variable: {}})}
			// setTimeout(run, 1000);	
		// })
	}

	render() {
		const { navigation } = this.props;
		return (
			<ImageBackground
				source={require('./resources/img/MainScreen.png')}
				imageStyle={{resizeMode: 'cover'}}
				style= {{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
			>
				<View style={{flex: 8}}/>
				<View style={{flex: 2, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<AnswerButton
							onPress= {() => {
								this.props.navigation.push('Question', {answered: [], variable: {}});
							}}
							style= {[styles.button]}
							answerText= {`Start`}
					/>
				</View>
			</ImageBackground>
		);
	}
}

class ResultCard extends React.Component {
	
	render() {
		const { price, desc, pacNum } = this.props
		return (
			<View
				style= {{
					marginVertical: 5,
					// height: '30%',
					width: '90%',
					backgroundColor: 'rgba(0, 0, 0, 0.7)',
					borderRadius: 20,
					alignItems: 'center',
				}}
			>
				<View
					style= {{
						flex: 6,
						flexDirection: 'row', 
					}}
				>
					<Image
						source= {require('./resources/img/Logo.png')}
						style= {{
								flex: 2,
								width: '100%',
								height: '100%',
								resizeMode: 'contain',
								// top: '-50%',
								// left: '-50%',
						}}
					/>
					<View style= {{flex: 8, justifyContent: 'center'}}>
						<Text style={
							{flex:6, color: 'white', fontSize: 20, textAlignVertical: 'center'}
						}> Package {pacNum} </Text>
						<Text
							style= {{color: 'white', textAlign: 'left', fontSize: 12, marginVertical: 10}}
						>
							{desc}
						</Text>
					</View>
				</View>
				<View
					style= {{
						flex: 4,
						// height: '100%',
						width: '100%',
						backgroundColor: 'rgba(0, 0, 0, 0.5)',
						borderBottomRightRadius: 20,
						borderBottomLeftRadius: 20,
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Text 
						style={{width: '100%',color: 'white', fontSize: 20, textAlignVertical: 'center', marginVertical: 10, marginHorizontal: 10}}
					>
							{`  Price      ${price}THB/month`}
					</Text>
				</View>
			</View>
		);
	}
}

class ResultScreen extends React.Component {
	static navigationOptions = {
		title: 'TERA',
        headerStyle: {
			backgroundColor: 'rgb( 40, 48, 84)',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
			flex: 1,
            alignSelf: 'center',
            textAlign: 'center',
        },
        headerRight: (<View></View>)
	}
	
	render() {
		const { navigation } = this.props;
		return (
			<ImageBackground
					source={require('./resources/img/Background.png')}
					imageStyle={{resizeMode: 'cover'}}
					style= {{ flex: 1, alignItems: 'center', justifyContent: 'center', zIndex: 5}}
			>	
				<View style={[{flex: 1, alignItems: 'center', justifyContent: 'center'}]}>
					<Text 
						style= {{
									fontSize: 30,
									color: 'rgba(204,238,251,0.9)',
						}}
					>	
						Our recommendations
					</Text>
				</View>
				<View style={[{flex: 7, width: '100%', justifyContent: 'center', alignItems: 'center'}]}>
						<ScrollView 
							style= {{flex: 1, width: '100%',}}
							contentContainerStyle= {{ justifyContent: 'center', alignItems: 'center' }}
						>
							<ResultCard 
								price= {'2,000'}
								pacNum= {1}
								desc= {
								`- Motor Insurance\n- Personal Accident Insurance\n- Micro-loan maximum 5,000 baht\n- Free financial literacy training`
								}
							/>
							<ResultCard 
								price= {'4,000'}
								pacNum= {2}
								desc= {
								`- Motor insurance\n- Personal accident insurance\n- Micro-loan maximum 10,000 baht\n- Health insurance\n- Life insurance\n- Free financial literacy training`
								}
							/>
							<ResultCard 
								price= {'6,000'}
								pacNum= {3}
								desc= {
								`- Motor insurance\n- Personal accident insurance\n- Micro-loan maximum 20,000 baht\n- Life insurance\n- Health insurance\n- Free financial literacy training`
								}
							/>
						</ScrollView>
				</View>
				<View style={[{flex: 1, justifyContent: 'center', alignItems: 'center'}]}>
					<AnswerButton 
						style={[styles.button]} 
						answerText='   Start over     ' 
						onPress={() => {navigation.navigate('Home')}}/>
				</View>
			</ImageBackground>
		)
	}
}

export default createStackNavigator({
	Home: HomeScreen,
	Question: QuestionScreen,
	Result: ResultScreen,
	Congrat: CongratScreen,
	MoreQuestions: ScrollDownQuestion,
}, { 
	headerMode: 'screen' 
},);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
	paddingVertical: 0,
	backgroundColor: 'rgb(0, 176, 240)',
	borderRadius: 20,
	width: '40%',
	alignItems: 'center',
	shadowColor: '#000',
	shadowOffset: {width: 0, height: 3},
	shadowOpacity: 0.8,
	shadowRadius: 2,
  }
});
