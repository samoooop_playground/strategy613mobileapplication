export default questions = {
    questions: [
        {
            question: 'How old are you?',
            choices: [
                '18-22 years old',
                '23-30 years old',
                '31-40 years old',
                '41-50 years old',   
            ]
        },{
            question: 'How much is your income?',
            choices: [
                'Less than 15,000 baht',
                '15,001 - 30,000 baht',
                '30,001 - 50,000 baht',
                'More than 50,000 baht',
            ]
        },{
            question: 'Where are you from?',
            choices: [
                'Bangkok',
                'Outside Bangkok',
            ]
        },{
            question: 'Do you own or rent a house?',
            choices: [
                'Own a house',
                'Rent a house',
                'Neither',
            ]
        },{
            question: 'Do you own or rent a $VEHICLE?',
            choices: [
                'Own a $VEHICLE',
                'Rent a $VEHICLE',
                'Neither',
            ]
        },{
            question: 'Do you send remittance home?',
            choices: [
                'Yes',
                'No',
            ]
        },{
            question: 'How much debt do you have?',
            choices: [
                'None',
                'Less than 15,000 baht',
                '15,001 - 30,000 baht',
                '30,001 - 50,001 baht',
                'More than 50,000 baht',
            ]
        },{
            question: 'How much savings do you have?',
            choices: [
                'None',
                'Less than 15,000 baht',
                '15,001 - 30,000 baht',
                '30,001 - 50,001 baht',
                'More than 50,000 baht',
            ]
        },{
            question: `What is your financial aspiration?`,
            choices: [
                'Buying a house',
                'Buying a car',
                'Both',
                'None',
            ],
            // multipleChoice: true,
        },{
            question: `Which financial products are you currently using?\n(You can select more than one)`,
            choices: [
                'Motor insurance',
                'Government health insurance',
                'Loan shark',  
            ],
            multipleChoice: true,
        }
    ]
}